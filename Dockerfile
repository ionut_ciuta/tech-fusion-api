FROM openjdk:8
ADD target/db-api.jar .
EXPOSE 9000
CMD java -jar db-api.jar