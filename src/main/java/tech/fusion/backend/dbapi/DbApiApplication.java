package tech.fusion.backend.dbapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import tech.fusion.backend.dbapi.generator.Generator;

@SpringBootApplication
public class DbApiApplication {

	@Autowired
	Generator generator;

	public static void main(String[] args) {
		SpringApplication.run(DbApiApplication.class, args);
	}

	@EventListener
	public void onApplicationStart(ContextRefreshedEvent event) {
		generator.generateAndInsertModels();
	}
}
