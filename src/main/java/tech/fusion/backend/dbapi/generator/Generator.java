package tech.fusion.backend.dbapi.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tech.fusion.backend.dbapi.model.*;
import tech.fusion.backend.dbapi.repository.*;

import java.util.*;

@Component
public class Generator {
    @Value("${data.size}")
    Integer SIZE;
    
    @Autowired
    CommunityRepository communityRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    TypeRepository typeRepository;

    @Autowired
    EventRepository eventRepository;

    String[] rooms = new String[] {
        "Thor",
        "Bunker",
        "Green Dot",
        "Spiderman",
        "Halo",
        "Half-Life",
        "Counter Strike",
        "Wolverine",
        "Time Stone",
        "Power Stone"
    };

    String[] ownerNames = new String[] {
            "Bruce Banner",
            "Steve Rogers",
            "Natasha Romanoff",
            "Stephen Strange",
            "Loki",
            "Fat Thor",
            "Tony Stark",
            "Pepper Pots",
            "Nick Furry",
            "Odin"
    };

    public boolean generateAndInsertModels() {
        List<Community> communities = new ArrayList<>();
        List<Location> locations = new ArrayList<>();
        List<Owner> owners = new ArrayList<>();
        List<Type> types = new ArrayList<>();
        List<Event> events = new ArrayList<>();
        
        for(int i = 0; i < SIZE; i++) {
            communities.add(new Community(
                    "id-com-" + i, "community-" + i
            ));
            
            locations.add(new Location(
                    "id-loc-" + i, rooms[i]
            ));
            
            owners.add(new Owner(
                    "id-owner-" + i, ownerNames[i]
            ));
            
            types.add(new Type(
                    "id-type-" + i, "type-" + i
            ));
            
            events.add(new Event(
                    "id-event-" + i,
                    "The Greatest Event-10" + i,
                    "descr. event " + i,
                    getStartDate(12 + i),
                    getEndDate(13 + i),
                    "id-type-" + i,
                    "id-com-" + i,
                    "id-owner-" + i,
                    "id-loc-" + i
            ));
        }

        communityRepository.deleteAll();
        locationRepository.deleteAll();
        ownerRepository.deleteAll();
        typeRepository.deleteAll();
        eventRepository.deleteAll();
        
        communities = communityRepository.insert(communities);
        locations = locationRepository.insert(locations);
        owners = ownerRepository.insert(owners);
        types = typeRepository.insert(types);
        events = eventRepository.insert(events);
        
        return communities.size() == SIZE
                && locations.size() == SIZE
                && owners.size() == SIZE
                && types.size() == SIZE
                && events.size() == SIZE;
    }

    private Date getStartDate(int day) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+2"));
        calendar.set(2020, Calendar.FEBRUARY, day, 12, 30);
        return calendar.getTime();
    }

    private Date getEndDate(int day) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+2"));
        calendar.set(2020, Calendar.FEBRUARY, day, 14, 0);
        return calendar.getTime();
    }
}
