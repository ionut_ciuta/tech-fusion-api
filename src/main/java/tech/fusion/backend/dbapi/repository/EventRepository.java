package tech.fusion.backend.dbapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.fusion.backend.dbapi.model.Event;

public interface EventRepository extends MongoRepository<Event, String> {
}
