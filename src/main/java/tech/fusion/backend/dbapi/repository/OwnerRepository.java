package tech.fusion.backend.dbapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.fusion.backend.dbapi.model.Community;
import tech.fusion.backend.dbapi.model.Owner;

public interface OwnerRepository extends MongoRepository<Owner, String> {
}
