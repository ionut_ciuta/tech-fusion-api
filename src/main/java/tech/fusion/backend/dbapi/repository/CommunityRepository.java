package tech.fusion.backend.dbapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.fusion.backend.dbapi.model.Community;

public interface CommunityRepository extends MongoRepository<Community, String> {
}
