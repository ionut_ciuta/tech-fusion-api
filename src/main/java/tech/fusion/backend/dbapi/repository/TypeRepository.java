package tech.fusion.backend.dbapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.fusion.backend.dbapi.model.Owner;
import tech.fusion.backend.dbapi.model.Type;

public interface TypeRepository extends MongoRepository<Type, String> {
}
