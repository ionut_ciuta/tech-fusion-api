package tech.fusion.backend.dbapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.fusion.backend.dbapi.model.Community;
import tech.fusion.backend.dbapi.model.Location;

public interface LocationRepository extends MongoRepository<Location, String> {
}
