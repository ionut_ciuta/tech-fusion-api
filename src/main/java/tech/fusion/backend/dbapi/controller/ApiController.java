package tech.fusion.backend.dbapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.fusion.backend.dbapi.model.*;
import tech.fusion.backend.dbapi.repository.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CommunityRepository communityRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    TypeRepository typeRepository;

    @GetMapping("/events")
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @GetMapping("/types")
    public List<Type> getAllTypes() {
        return typeRepository.findAll();
    }

    @GetMapping("/owners")
    public List<Owner> getAllOwners() {
        return ownerRepository.findAll();
    }

    @GetMapping("/locations")
    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    @GetMapping("/communities")
    public List<Community> getAllCommunities() {
        return communityRepository.findAll();
    }
}
