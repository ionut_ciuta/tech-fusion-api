package tech.fusion.backend.dbapi.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Community extends AModel {

    public Community() {
    }

    public Community(String id, String name) {
        super(id, name);
    }
}
