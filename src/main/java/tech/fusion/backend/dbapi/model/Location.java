package tech.fusion.backend.dbapi.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Location extends AModel {

    public Location() {
    }

    public Location(String id, String name) {
        super(id, name);
    }
}
