package tech.fusion.backend.dbapi.model;

import com.mongodb.lang.NonNull;
import com.mongodb.lang.Nullable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Document
public class Event extends AModel {
    @Nullable
    public String description;

    @NonNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public Date startDate;

    @NonNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public Date endDate;

    @NonNull
    public String typeId;

    @Nullable
    public String communityId;

    @NonNull
    public String ownerId;

    @NonNull
    public String locationId;

    public Event() {
    }

    public Event(String id, String name, @Nullable String description, @NonNull Date startDate, @NonNull Date endDate,
                 @NonNull String typeId, @Nullable String communityId, @NonNull String ownerId, @NonNull String locationId) {
        super(id, name);
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.typeId = typeId;
        this.communityId = communityId;
        this.ownerId = ownerId;
        this.locationId = locationId;
    }
}
