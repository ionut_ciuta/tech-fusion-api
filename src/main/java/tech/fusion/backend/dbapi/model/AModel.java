package tech.fusion.backend.dbapi.model;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;

public abstract class AModel {
    @Id
    public String id;

    @NonNull
    public String name;

    public AModel() {
    }

    public AModel(String id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }
}
