package tech.fusion.backend.dbapi.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Type extends AModel {

    public Type() {
    }

    public Type(String id, String name) {
        super(id, name);
    }
}
