package tech.fusion.backend.dbapi.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Owner extends AModel {

    public Owner() {
    }

    public Owner(String id, String name) {
        super(id, name);
    }
}
