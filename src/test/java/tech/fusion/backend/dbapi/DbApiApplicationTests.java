package tech.fusion.backend.dbapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import tech.fusion.backend.dbapi.model.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DbApiApplicationTests {
	private final ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	MockMvc mockMvc;

	@Value("${data.size}")
	Integer size;

	@Test
	void test() throws Exception {
		String result;

		result = testGet("/api/locations");
		Location[] locations = objectMapper.readValue(result, Location[].class);
		assertEquals(size, locations.length);

		result = testGet("/api/types");
		Type[] types = objectMapper.readValue(result, Type[].class);
		assertEquals(size, types.length);

		result = testGet("/api/owners");
		Owner[] owners = objectMapper.readValue(result, Owner[].class);
		assertEquals(size, owners.length);

		result = testGet("/api/communities");
		Community[] communities = objectMapper.readValue(result, Community[].class);
		assertEquals(size, communities.length);

		result = testGet("/api/events");
		Event[] events = objectMapper.readValue(result, Event[].class);
		assertEquals(size, events.length);

		for (int i = 0; i < size; i++) {
			assertEquals(owners[i].id, events[i].ownerId);
			assertEquals(types[i].id, events[i].typeId);
			assertEquals(communities[i].id, events[i].communityId);
			assertEquals(locations[i].id, events[i].locationId);
		}
	}

	private String testGet(String url) throws Exception {
		 return mockMvc
				 .perform(get(url))
				 .andDo(print())
				 .andExpect(status().isOk())
				 .andReturn()
				 .getResponse()
				 .getContentAsString();
	}
}
