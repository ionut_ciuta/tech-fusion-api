#!/bin/bash

# Write decoded pem
echo $AWS_PEM | base64 --decode > cert.pem
chmod 400 cert.pem

ssh -o StrictHostKeyChecking=no -i cert.pem $AWS_USER@$AWS_IP '\
  docker-compose down; yes | docker system prune
'
