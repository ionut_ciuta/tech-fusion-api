#!/bin/bash

# Write pem file
echo $AWS_PEM | base64 --decode > cert.pem
# Set permissions for pem file
chmod 400 cert.pem
cat cert.pem

echo 'Copying tgz to AWS'
echo '.....'
scp -o StrictHostKeyChecking=no -i cert.pem docker-compose.yml $AWS_USER@$AWS_IP:/home/$AWS_USER/

DOCKER_LOGIN_CMD="docker login registry.gitlab.com -u $DOCKER_USER -p $DOCKER_PASS"
ssh -o StrictHostKeyChecking=no -i cert.pem $AWS_USER@$AWS_IP $DOCKER_LOGIN_CMD

ssh -o StrictHostKeyChecking=no -i cert.pem $AWS_USER@$AWS_IP '\
  docker-compose down --rmi all;\
  docker-compose up -d;
'